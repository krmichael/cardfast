const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const consign = require('consign');

module.exports = function() {
  
  app.use(bodyParser.json());
  
  consign()
    .include('routes')
    .into(app);

  return app;
}