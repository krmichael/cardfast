const { check, validationResult } = require('express-validator/check');

module.exports = function(app) {

  app.post('/cartoes/autoriza', [
    check('numero', 'Número é obrigátorio e deve ter 16 caractes').not().isEmpty().isLength(16),
    check('bandeira', 'Bandeira do cartão é obrigátoria').not().isEmpty(),
    check('ano_de_expiracao', 'Ano de expiração é obrigátorio').not().isEmpty().isLength(2,4),
    check('mes_de_expiracao', 'Mês de expiração é obrigátorio').not().isEmpty().isLength(2),
    check('cvv', 'CVV é obrigátorio e deve ter 3 caracteres').not().isEmpty().isLength(3)
  ], function(req, res) {
    
    const erros = validationResult(req);

    if(!erros.isEmpty()) {
      console.log('Erros de validação foram encontrados');
      res.status(400).json({erros: erros.array()});
      return;
    }

    console.log('Processando pagamento com cartão');

    let cartao = req.body;
    cartao.status = "Autorizado";

    let response = {
      dados_do_cartao: cartao
    }

    res.status(201).json(response);

  });
}